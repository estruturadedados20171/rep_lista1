package questao2

/**
  * Created by mdcc on 06/04/17.
  */
class SortedLinkedList extends AbstractLinkedList{

  override def inserirAbs(valor: Int): Unit = {
    if (qtd == 0){
      val node = new Node(valor)
      primeiro = node
    }else{
      if (valor < primeiro.valor){
        val node = new Node(valor, primeiro)
        primeiro = node
      }else{
        var pai = primeiro
        var node = primeiro.proximo
        while ( node != null ){
          if (node.valor > valor){
            val myNode = new Node(valor, node)
            pai.proximo = myNode
          }
          pai = node
          node = node.proximo
        }
        val myNode = new Node(valor)
        pai.proximo = myNode
      }
    }
  }

  def isEqualTo(list:SortedLinkedList):Int = {
    if (qtd != list.qtd)
      return 0
    var myNode = primeiro
    var hisNode = list.primeiro
    var i:Int = 0
    while ((myNode != null) && (hisNode != null) && (myNode.valor == hisNode.valor)){
      myNode = myNode.proximo
      hisNode = hisNode.proximo
      i += 1
    }
    if (i == qtd)
      return 1
    else
      return 0
  }
}
