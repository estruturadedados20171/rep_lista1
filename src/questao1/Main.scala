package questao1

/**
  * Created by mdcc on 05/04/17.
  */
object Main extends App{
  val myList = new LinkedList();

  println("Is empty: "+myList.isEmpty())

  myList.inserir(5)
  myList.inserir(4)
  myList.inserir(3)
  myList.inserir(2)
  myList.inserir(19)
  myList.inserir(37)


  println("List:")
  myList.print()
  println("List:")
  myList.printRecursivo()
  println("List Reverso:")
  myList.printRecursivoRev()

  println("Is empty: "+myList.isEmpty())

  println("id 1 = "+myList.recuperar(1))
  println("id 20 = "+myList.recuperar(20))
  println("id do elemento 5 = "+myList.buscar(5))

  myList.removerRec(3)
  myList.print()
  myList.removerRec(19)
  myList.print()

  myList.liberar()
  println("Lista vazia?")
  myList.print() // Observe que nada é impresso entre o "Lista vazia?" e o "Is empty: 1"
  println("Is empty: "+myList.isEmpty())
}
