package questao1

/**
  * Created by mdcc on 05/04/17.
  */
class LinkedList extends AbstractLinkedList{

  override def inserirAbs(valor: Int): Unit = {
    val node = new Node(valor, primeiro)
    primeiro = node
  }
}
