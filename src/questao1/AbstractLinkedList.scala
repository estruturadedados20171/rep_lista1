package questao1

/**
  * Created by mdcc on 05/04/17.
  */
trait AbstractLinkedList {
  var primeiro:Node = null
  var qtd:Int = 0

  def inserir(valor:Int):Unit = {
    qtd = qtd + 1
    inserirAbs(valor)
  }

  def inserirAbs(valor:Int):Unit

  def print():Unit = {
    var elemento = primeiro
    while (elemento != null){
      println(elemento.valor)
      elemento = elemento.proximo
    }
  }

  def printRecursivo():Unit = {
    printRecursivoAux(primeiro)
  }

  private def printRecursivoAux(elemento:Node):Unit = {
    println(elemento.valor)
    if (elemento.proximo != null){
      printRecursivoAux(elemento.proximo)
    }
  }

  def printRecursivoRev():Unit = {
    printRecursivoRevAux(primeiro)
  }

  private def printRecursivoRevAux(elemento:Node):Unit = {
    if (elemento.proximo != null){
      printRecursivoRevAux(elemento.proximo)
    }
    println(elemento.valor)
  }

  def isEmpty():Int = {
    if (qtd == 0)
      1
    else
      0
  }

  def recuperar(id:Int):Int = {
    //Se id > qtd, retorna o último
    var myId = id
    if (id > qtd){
      myId = qtd
    }
    var i = 1
    var elemento = primeiro
    while (i < myId) {
      i = i + 1
      elemento = elemento.proximo
    }
    elemento.valor
  }

  def buscar(valor:Int):Int = {
    var i = 0
    var elemento = primeiro
    while (elemento != null) {
      i = i + 1
      if (elemento.valor == valor){
        return i
      }
      elemento = elemento.proximo
    }
    -1
  }

  def remover(valor:Int):Unit = {
    val id = buscar(valor)
    if (id == 1) {
      primeiro = primeiro.proximo
      qtd = qtd - 1
    }
    else if (id > 1){
      var pai = primeiro
      var node = primeiro.proximo
      var i = 2
      while (i < id){
        i = i + 1
        pai = node
        node = node.proximo
      }
      pai.proximo = node.proximo
      qtd = qtd - 1
    }
  }

  def removerRec(valor:Int):Unit = {
    if (primeiro.valor == valor) {
      primeiro = primeiro.proximo
      qtd = qtd - 1
    }
    else{
      val pai = primeiro
      val node = primeiro.proximo
      removerRecAux(valor, pai, node)
    }
  }

  private def removerRecAux(valor: Int, pai: Node, node: Node): Unit = {
    if (node.valor == valor) {
      pai.proximo = node.proximo
      qtd = qtd - 1
    }
    else
      removerRecAux(valor,node,node.proximo)
  }

  def liberar():Unit = {
    primeiro = null
    qtd = 0
  }

}
