package tutorial

/**
  * Created by mdcc on 05/04/17.
  */
class ConcreteNode(val atributo1:Int, val atributo2:Int = 5) extends AbstractNode{
  override def soma2(): Int = {
    atributo1 + 3 * atributo2
  }
}