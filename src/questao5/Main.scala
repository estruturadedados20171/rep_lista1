package questao5

/**
  * Created by mdcc on 08/04/17.
  */
object Main extends App{

  println("1) Cria lista vazia")
  val list = new CircularLinkedList()

  println("2) Inserir elemento (no topo)")
  list.inserir(1)
  list.inserir(2)
  list.inserir(53)
  list.inserir(3)
  list.inserir(4)

  println("3) Imprimir os valores armazenados na lista")
  list.print()

  println("4) Imprimir recursivamente os valores armazenados na lista")
  list.printRec()

  println("5) Lista vazia? "+list.estaVazia())

  println("6.1) Buscar id do elemento 3: "+list.buscar(3))
  println("6.2) Recuperar valor do 3º elemento da lista: "+list.recuperar(3))

  println("7) Remover elemento 2")
  list.remover(2)
  list.print()

  println("8) Remover recursivamente elemento 4")
  list.removerRec(4)
  list.print()

  println("9) Liberar a lista")
  list.liberar()
  list.print()
  println("Lista vazia? "+list.estaVazia())
}
