package questao5

/**
  * Created by mdcc on 08/04/17.
  */
class CircularLinkedList(var primeiro:Node = null, var ultimo:Node = null, var qtd:Int = 0) {

  def inserir(valor:Int):Unit = {
    if (qtd == 0){
      val node = new Node(valor)
      node.anterior = node
      node.proximo  = node
      primeiro = node
      ultimo   = node
    }else{
      val node = new Node(valor,ultimo,primeiro)
      primeiro.anterior = node
      ultimo.proximo    = node
      primeiro          = node
    }
    qtd += 1
  }

  def print():Unit = {
    var node = primeiro
    for (i <- 1 to qtd){
      println("["+node.anterior.valor+"] => "+ node.valor+" => ["+node.proximo.valor+"]")
      node = node.proximo
    }
  }

  def printRec():Unit = {
    printRecAux(primeiro, qtd)
  }

  private def printRecAux(node:Node, falta:Int):Unit = {
    if (falta <= 0)
      return
    println("["+node.anterior.valor+"] => "+ node.valor+" => ["+node.proximo.valor+"]")
    printRecAux(node.proximo, falta - 1)
  }

  def estaVazia():Int = {
    if (qtd == 0)
      1
    else
      0
  }

  def buscar(valor:Int):Int = {
    var node = primeiro
    var i = 0
    for (i <- 1 to qtd){
      if (node.valor == valor)
        return i
      node = node.proximo
    }
    return -1
  }

  def recuperar(nodeId:Int):Int = {
    if ( (nodeId <= 0) || (qtd == 0))
      return -1
    var node = primeiro
    var i = 0
    for (i <- 1 to nodeId-1){
      node = node.proximo
    }
    return node.valor
  }

  def remover(valor:Int):Unit = {
    val nodeId = buscar(valor)
    if (nodeId <= 0)
      return
    if (qtd == 1){
      primeiro = null
      ultimo   = null
      qtd = 0
      return
    }
    var node = primeiro
    for (i <- 1 to nodeId-1){
      node = node.proximo
    }
    val pai = node.anterior
    val filho = node.proximo
    pai.proximo = filho
    filho.anterior = pai
    if (nodeId == 1)
      primeiro = filho
    if (nodeId == qtd)
      ultimo = pai
    qtd -= 1
  }

  def removerRec(valor:Int):Unit = {
    if (qtd == 0)
      return
    if (qtd == 1){
      primeiro = null
      ultimo   = null
      qtd = 0
      return
    }
    removerRecAux(valor, primeiro, qtd)
  }

  private def removerRecAux(valor:Int, node:Node, falta:Int):Unit = {
    if (falta <= 0)
      return
    if (node.valor == valor){
      val pai = node.anterior
      val filho = node.proximo
      pai.proximo = filho
      filho.anterior = pai
      if (falta == qtd)
        primeiro = filho
      if (falta == 1)
        ultimo = pai
      qtd -= 1
      return
    }
    removerRecAux(valor, node.proximo, falta-1)
  }

  def liberar():Unit = {
    primeiro = null
    ultimo = null
    qtd = 0
  }


}
