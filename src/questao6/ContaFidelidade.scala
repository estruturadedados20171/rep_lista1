package questao6

/**
  * Created by mdcc on 08/04/17.
  */
class ContaFidelidade(val numero:Int, var saldo:Float = 0, var bonus:Float = 0) extends Conta{

  override def credito(valor: Float): Unit = {
    super.credito(valor)
    bonus += valor * 0.01f
  }

  def renderBonus():Unit = {
    saldo += bonus
    bonus = 0
  }

}
