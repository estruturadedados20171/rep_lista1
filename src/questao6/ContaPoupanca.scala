package questao6

/**
  * Created by mdcc on 08/04/17.
  */
class ContaPoupanca(val numero:Int, var saldo:Float = 0) extends Conta{

  def renderJuros(aliquota:Float):Unit = {
    saldo += saldo * aliquota
  }

}
