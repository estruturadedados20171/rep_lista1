package questao6

/**
  * Created by mdcc on 08/04/17.
  */
class Banco(val contas:SortedLinkedList = new SortedLinkedList()) {

  def inserirContaCorrente(numero:Int): Unit ={
    contas.inserir(new ContaCorrente(numero))
  }

  def inserirContaPoupanca(numero:Int): Unit ={
    contas.inserir(new ContaPoupanca(numero))
  }

  def inserirContaFidelidade(numero:Int): Unit ={
    contas.inserir(new ContaFidelidade(numero))
  }

  def credito(id:Int, valor:Float): Unit ={
    val conta = contas.buscarConta(id)
    if (conta != null)
      conta.credito(valor)
  }

  def debito(id:Int, valor:Float): Unit ={
    val conta = contas.buscarConta(id)
    if (conta != null)
      conta.debito(valor)
  }

  def saldo(id:Int): Float ={
    val conta = contas.buscarConta(id)
    if (conta != null)
      return conta.saldo
    return 0f
  }

  def bonus(id:Int):Float = {
    val conta = contas.buscarConta(id)
    if (conta == null)
      return 0f
    if (conta.isInstanceOf[ContaFidelidade]){
      return conta.asInstanceOf[ContaFidelidade].bonus
    }else{
      return 0f
    }
  }

  def transferencia(de:Int, para:Int, valor:Float): Unit ={
    val from = contas.buscarConta(de)
    val to   = contas.buscarConta(para)
    if ((from == null) || (to == null) || (from.saldo < valor))
      return
    from.debito(valor)
    to.credito(valor)
  }

  def renderJuros(id:Int, aliquota:Float):Unit = {
    val conta = contas.buscarConta(id)
    if (conta == null)
      return
    if (conta.isInstanceOf[ContaPoupanca]){
      conta.asInstanceOf[ContaPoupanca].renderJuros(aliquota)
    }
  }

  def renderBonus(id:Int):Unit = {
    val conta = contas.buscarConta(id)
    if (conta == null)
      return
    if (conta.isInstanceOf[ContaFidelidade]){
      conta.asInstanceOf[ContaFidelidade].renderBonus()
    }
  }

  def remover(id:Int): Unit ={
    contas.remover(id)
  }

  def imprimir(): Unit ={
    contas.print()
  }

}
