package questao6

/**
  * Created by mdcc on 08/04/17.
  */
object Main extends App{

  val banco = new Banco()

  banco.inserirContaCorrente(17)
  banco.inserirContaCorrente(30)
  banco.inserirContaPoupanca(48)
  banco.inserirContaFidelidade(12)
  banco.credito(17, 120f)
  banco.debito(17,35f)
  println(banco.saldo(17))
  banco.credito(12,100f)
  println(banco.bonus(12))
  banco.transferencia(17,48,50f)
  banco.renderJuros(48, 0.05f)
  banco.renderBonus(12)
  banco.remover(30)
  banco.imprimir()



//  println("1) Criar uma lista vazia")
//  val list = new SortedLinkedList()
//
//  println("2) Inserir elementos em ordem")
//  list.inserir(new ContaCorrente(1))
//  list.inserir(new ContaPoupanca(3))
//  list.inserir(new ContaFidelidade(5))
//  list.inserir(new ContaCorrente(19))
//  list.inserir(new ContaPoupanca(2))
//  list.inserir(new ContaCorrente(4))
//
//  println("3) Imprimir valores armazenados")
//  list.print()
//
//  println("4) Imprimir recursivamente os valores armazenados")
//  list.printRec()
//
//  println("5) Imprimir em ordem reversa os valores armazenados")
//  list.printRev()
//
//  println("6) Lista está vazia? "+list.estaVazia())
//
//  println("7) Buscar conta pelo id")
//  val c = list.buscarConta(3)
//  c.credito(120)
//  c.debito(85)
//  println(c)
//
//  println("8) Remover elemento 4")
//  list.remover(4)
//  list.print()
//
//  println("9) Remover recursivamente elemento 2")
//  list.remover(2)
//  list.print()
//
//  println("10) Liberar a lista")
//  list.liberar()
//  list.print() //Nada é impresso aqui
//  println("Lista está vazia? "+list.estaVazia())

}
