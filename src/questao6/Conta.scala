package questao6

/**
  * Created by mdcc on 08/04/17.
  */
trait Conta {
  val numero:Int
  var saldo:Float

  def credito(valor:Float):Unit = {
    saldo += valor
  }

  def debito(valor:Float):Unit = {
    if (valor <= saldo)
      saldo -= valor
    else
      println("Saldo insuficiente")
  }

  override def toString: String = {
    return "número: "+numero+", Saldo R$ "+saldo
  }
}
