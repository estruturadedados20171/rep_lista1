package questao6

/**
  * Created by mdcc on 08/04/17.
  */
class SortedLinkedList(var primeiro:Node = null,var qtd:Int = 0) {

  def inserir(conta: Conta):Unit = {
    if (qtd == 0){
      primeiro = new Node(conta)
      qtd = 1
      return
    }
    if (conta.numero < primeiro.conta.numero){
      val node = new Node(conta, primeiro)
      primeiro = node
      return
    }
    var node = primeiro
    while (node != null){
      if (node.proximo == null){
        node.proximo = new Node(conta)
        qtd += 1
        return
      }
      if (node.proximo.conta.numero > conta.numero){
        node.proximo = new Node(conta,node.proximo)
        qtd += 1
        return
      }
      node = node.proximo
    }
  }

  def print():Unit = {
    var node = primeiro
    while (node != null){
      println(node.conta)
      node = node.proximo
    }
  }

  def printRec():Unit = {
    printRecAux(primeiro)
  }

  private def printRecAux(node:Node):Unit = {
    if (node == null)
      return
    println(node.conta)
    printRecAux(node.proximo)
  }

  def printRev():Unit = {
    printRevAux(primeiro)
  }

  private def printRevAux(node:Node):Unit = {
    if (node == null)
      return
    printRevAux(node.proximo)
    println(node.conta)
  }

  def estaVazia():Int = {
    if (qtd == 0)
      1
    else
      0
  }

  def buscarConta(id:Int):Conta = {
    var node = primeiro
    while (node != null){
      if (node.conta.numero == id)
        return node.conta
      node = node.proximo
    }
    return null
  }

  def remover(id:Int):Unit = {
    if (qtd == 0)
      return
    if (primeiro.conta.numero == id) {
      primeiro = primeiro.proximo
      qtd -= 1
    }
    var node = primeiro
    while (node != null){
      if (node.proximo.conta.numero > id)
        return
      if (node.proximo.conta.numero == id){
        node.proximo = node.proximo.proximo
        qtd -= 1
        return
      }
      node = node.proximo
    }
  }

  def removerRec(id:Int):Unit = {
    if (qtd == 0)
      return
    if (primeiro.conta.numero == id) {
      primeiro = primeiro.proximo
      qtd -= 1
      return
    }
    removerRecAux(id, primeiro)
  }

  private def removerRecAux(id:Int, node:Node): Unit ={
    if (node == null)
      return
    if (node.proximo.conta.numero == id){
      node.proximo = node.proximo.proximo
      qtd -= 1
    }else if (node.proximo.conta.numero > id){
      return
    }else{
      removerRecAux(id, node.proximo)
    }
  }

  def liberar():Unit = {
    primeiro = null
    qtd = 0
  }

}
