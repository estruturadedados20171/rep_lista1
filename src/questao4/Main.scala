package questao4

/**
  * Created by mdcc on 08/04/17.
  */
object Main extends App{
  val list = new CircularLinkedList()
  list.inserir(1)
  list.inserir(2)
  list.inserir(19)
  list.inserir(3)
  list.inserir(4)

  println("lista.print")
  list.print()
  println("lista.printRec")
  list.printRec()

  println("Está vazia? "+list.estaVazia())
  println("Id do 3: "+list.buscar(3))
  println("3º elemento: "+list.recuperar(3))

  println("remover 2")
  list.remove(2)
  list.print()

  println("remover recursivamente 3")
  list.removeRec(3)
  list.print()

  println("Liberar lista")
  list.liberar()
  println("Está vazia? "+list.estaVazia())
}
