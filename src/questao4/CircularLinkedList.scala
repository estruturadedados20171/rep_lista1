package questao4

/**
  * Created by mdcc on 08/04/17.
  */
class CircularLinkedList(var primeiro:Node = null, var ultimo:Node = null, var qtd:Int = 0) {

  def inserir(valor:Int):Unit = {
    if (qtd == 0){
      val node = new Node(valor)
      node.proximo = node
      primeiro = node
      ultimo   = node
    }else{
      val node = new Node(valor, primeiro)
      ultimo.proximo = node
      primeiro = node
    }
    qtd += 1
  }

  def print():Unit = {
    var node = primeiro
    for (i <- 1 to qtd){
      println(node.valor+" => ["+node.proximo.valor+"]")
      node = node.proximo
    }
  }

  def printRec():Unit = {
    printRecAux(primeiro, qtd)
  }

  private def printRecAux(node:Node, falta:Int):Unit = {
    if (falta <= 0)
      return
    println(node.valor+" => ["+node.proximo.valor+"]")
    printRecAux(node.proximo, falta - 1)
  }

  def estaVazia():Int = {
    if (qtd == 0)
      1
    else
      0
  }

  def buscar(valor:Int):Int = {
    var node = primeiro
    var i = 0
    for (i <- 1 to qtd){
      if (node.valor == valor)
        return i
      node = node.proximo
    }
    return -1
  }

  def recuperar(nodeId:Int):Int = {
    if ( (nodeId <= 0) || (qtd == 0))
      return -1
    var node = primeiro
    var i = 0
    for (i <- 1 to nodeId-1){
      node = node.proximo
    }
    return node.valor
  }


  def remove(valor:Int):Unit = {
    val nodeId = buscar(valor)
    if (nodeId > 0){
      if (qtd == 1){
        primeiro = null
        ultimo   = null
        qtd = 0
        return
      }
      if (nodeId == 1){
        ultimo.proximo = primeiro.proximo
        primeiro       = primeiro.proximo
        qtd -= 1
      }
      var pai = primeiro
      var node = primeiro.proximo
      var i = 0
      for (i <- 2 to nodeId-1){
        pai  = node
        node = pai.proximo
      }
      if (nodeId == qtd){
        ultimo = pai
        pai.proximo = primeiro
        qtd -= 1
      }
      pai.proximo = node.proximo
      qtd -= 1
    }
  }

  def removeRec(valor:Int):Unit = {
    if (primeiro == null)
      return
    if (primeiro.valor == valor){
      ultimo.proximo = primeiro.proximo
      primeiro = primeiro.proximo
      qtd -= 1
      return
    }
    if (ultimo.valor == valor){
      var node = primeiro
      var i = 0
      for (i <- 1 to qtd-1){
        node = node.proximo
      }
      node.proximo = primeiro
      qtd -= 1
    }
    removeRecAux(valor, primeiro.proximo, primeiro, qtd-2)
  }

  def removeRecAux(valor:Int,node:Node, pai:Node, falta:Int):Unit = {
    if (falta <= 0)
      return
    if (node.valor == valor){
      pai.proximo = node.proximo
      qtd -= 1
    }else{
      removeRecAux(valor, node.proximo, node, falta-1)
    }
  }

  def liberar():Unit = {
    primeiro = null
    ultimo = null
    qtd = 0
  }

}