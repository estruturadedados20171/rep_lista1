package questao3

/**
  * Created by mdcc on 05/04/17.
  */
class SortedLinkedList extends AbstractLinkedList{

  override def inserirAbs(valor: Int): Unit = {
    if (qtd == 0){
      primeiro = new Node(valor)
    }else{
      if (primeiro.valor > valor){
        val node = new Node(valor,primeiro,null)
        primeiro.anterior = node
        primeiro = node
        return
      }
      var elemento = primeiro
      while (elemento != null){

        if (elemento.valor > valor){
          val node = new Node(valor,elemento.anterior,elemento)
          elemento.anterior.proximo = node
          elemento.anterior = node
          return
        }

        if (elemento.proximo == null){
          val node = new Node(valor,null,elemento)
          elemento.proximo = node
          return
        }
        elemento = elemento.proximo
      }

    }
  }

  override def remover(valor: Int): Unit = {
    val idNode = buscar(valor)
    if (idNode <= 0)
      return
    if (idNode == 1){
      primeiro.proximo.anterior = null
      primeiro = primeiro.proximo
      qtd = qtd - 1
    }else{
      var i = 2
      var node = primeiro.proximo
      while (i < idNode){
        i = i + 1
        node = node.proximo
      }
      node.anterior.proximo = node.proximo
      if (node.proximo != null)
        node.proximo.anterior = node.anterior
      qtd = qtd - 1
    }
  }

  override def removerRec(valor: Int): Unit = {
    val idNode = buscar(valor)
    if (idNode <= 0)
      return
    if (idNode == 1){
      primeiro.proximo.anterior = null
      primeiro = primeiro.proximo
      qtd = qtd - 1
    }else{
      removerRecAux(valor, primeiro.proximo)
    }
  }

  def removerRecAux(valor:Int, node:Node):Unit = {
    if (node == null)
      return
    if (node.valor == valor){
      node.anterior.proximo = node.proximo
      if (node.proximo != null)
        node.proximo.anterior = node.anterior
      qtd = qtd - 1
    }
    removerRecAux(valor, node.proximo)
  }

  def isEqualTo(list:SortedLinkedList):Int = {
    if (qtd != list.qtd)
      return 0
    var myNode = primeiro
    var hisNode = list.primeiro
    var i:Int = 0
    while ((myNode != null) && (hisNode != null) && (myNode.valor == hisNode.valor)){
      myNode = myNode.proximo
      hisNode = hisNode.proximo
      i += 1
    }
    if (i == qtd)
      return 1
    else
      return 0
  }
}
