package questao3

/**
  * Created by mdcc on 05/04/17.
  */
object Main extends App{
  val myList = new SortedLinkedList();

  println("Is empty: "+myList.isEmpty())

  myList.inserir(5)
  myList.inserir(4)
  myList.inserir(3)
  myList.inserir(2)
  myList.inserir(19)
  myList.inserir(37)


  println("List:")
  myList.print()
  println("List:")
  myList.printRecursivo()
  println("List Reverso:")
  myList.printRecursivoRev()

  println("Is empty: "+myList.isEmpty())

  println("id 1 = "+myList.recuperar(1))
  println("id 20 = "+myList.recuperar(20))
  println("id do elemento 5 = "+myList.buscar(5))

  println("Lista antes das remoções")
  myList.print()
  println("Remover 3")
  myList.remover(3)
  myList.print()
  println("Remover 19 recursivamente")
  myList.removerRec(19)
  myList.print()

  myList.liberar()
  println("Lista vazia?")
  myList.print() // Observe que nada é impresso entre o "Lista vazia?" e o "Is empty: 1"
  println("Is empty: "+myList.isEmpty())


  println("Verificar se listas são iguais")
  println("Consideramos iguais listas com os mesmos elementos")
  println("(comentar linha 41 para testar)")
  val myList2 = new SortedLinkedList()


  myList2.inserir(5)
  myList2.inserir(4)
//  myList2.inserir(3)
  myList2.inserir(2)
  myList2.inserir(37)

  println("Is equal to myList2? "+myList.isEqualTo(myList2))

  myList.print()
  myList2.print()
}
